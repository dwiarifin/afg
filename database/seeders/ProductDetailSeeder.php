<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class ProductDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        // Product
        for ($i = 1; $i <= 10; $i++) {
            // Insert Faker to Category
            DB::table('product_details')->insert([
                'product_id' => mt_rand(1, 10),
                'sku' => $faker->randomNumber(5),
                'supplier' => $faker->text(10),
                'location' => $faker->text(25),
                'price' => $faker->randomDigit(),
                'quantity' => $faker->randomDigit(),
                'created_at' => Carbon::now()
            ]);
        }
    }
}
