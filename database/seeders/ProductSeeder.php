<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        // Product
        for ($i = 1; $i <= 10; $i++) {
            // Insert Faker to Category
            DB::table('products')->insert([
                'category_id' => $faker->numberBetween(1, 10),
                'name' => $faker->text(25),
                'description' => $faker->text(50),
                'created_at' => Carbon::now()
            ]);
        }
    }
}
