<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = Faker::create();

        for ($i = 1; $i <= 10; $i++) {
            // Insert Faker to Category
            DB::table('categories')->insert([
                'category_name' => $faker->word(10),
                'created_at' => Carbon::now()
            ]);
        }
    }
}
