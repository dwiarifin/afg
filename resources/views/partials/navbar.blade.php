<header>
    <nav class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar bg-dark">
        <div class="container-fluid px-5">
          <a class="navbar-brand" href="#">
              <img src="assets/img/logonav.png" />
          </a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">ABOUT</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">STORE</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">GAMEBOX</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">TOKEN</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">STAKING</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">DOCS</a>
              </li>
            </ul>
            <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
                <li class="nav-item">
                  <a class="nav-link" href="#" target="_blank" rel="noopener">REGISTER</a>
                </li>
                <li class="nav-item mx-0">
                  <a class="btn btn-primary rounded">LOGIN</a>
                </li>
              </ul>
          </div>
        </div>
      </nav>
  </header>