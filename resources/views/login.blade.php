@extends('layouts.public')

@section('container')
<section class="vh-100 mt-5">
    <div class="container-fluid h-custom">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-md-9 col-lg-6 col-xl-5">
          <img src="assets/img/Illustration.png" class="img-fluid"
            alt="Sample image">
        </div>
        <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
          <form>
            <div class="justify-content-lg-start">
              <img src="assets/img/logo.png" />
              <br>
              <br>
            </div>
            <!-- Email input -->
            <div class="form-outline mb-4">
                <button type="button" class="form-control btn btn-light rounded submit px-3 shadow"
                style="padding-left: 0.5rem; padding-right: 0.5rem;"><i class="fa-brands fa-google"></i> Login with Google</button>
            </div>
            <div class="form-outline mb-4">
                <button type="button" class="form-control btn btn-light rounded submit px-3 shadow"
                style="padding-left: 0.5rem; padding-right: 0.5rem;"> <i class="fa-brands fa-facebook-f"></i> Login with Facebook</button>
            </div>
            <div class="divider d-flex align-items-center my-4">
              <p class="text-center fw-bold mx-3 mb-0">Or</p>
            </div>
  
            <!-- Email input -->
            <div class="form-outline mb-4">
              <input type="email"class="form-control form-control-lg"
                placeholder="example@gmail.com" />
            </div>
  
            <!-- Password input -->
            <div class="form-outline mb-3">
              <input type="password" class="form-control form-control-lg"
                placeholder="**********" />
            </div>
  
            <div class="d-flex justify-content-between align-items-center">
              <!-- Checkbox -->
              <div class="form-check mb-0">
                <input class="form-check-input me-2" type="checkbox" value="" id="form2Example3" />
                <label class="form-check-label">
                  Remember me
                </label>
              </div>
              <a href="#!" class="text-body">Forgot password?</a>
            </div>
  
            <div class="text-center text-lg-start mt-4 pt-2">
              <button type="button" class="form-control btn btn-primary rounded submit px-3"
                style="padding-left: 0.5rem; padding-right: 0.5rem;">Login</button>
              <p class="small fw-bold mt-2 pt-1 mb-0 text-center">Don't have an account? <a href="#!"
                  class="link-danger">Register</a></p>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
@endsection