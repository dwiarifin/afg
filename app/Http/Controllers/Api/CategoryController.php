<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Resources\ApiResource;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    //

    public function index(Request $request)
    {
        $categories = DB::table('categories')->latest()->paginate(5);
        return new ApiResource(true, 'List Data Category', $categories);
    }

    public function show(Category $category)
    {
        return new ApiResource(true, 'Data Product has been found', $category);
    }
}
