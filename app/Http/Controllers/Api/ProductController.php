<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Resources\ApiResource;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    //

    public function index(Request $request)
    {

        $sortBy = $request->query('sort');
        $groupBy = $request->query('group');
        if ($sortBy) {
            $products = DB::table('products')->orderBy($sortBy)->get();
        } else if ($groupBy) {
            $products = DB::table('products')->orderBy($sortBy)->get()->groupBy($groupBy);
        } else {
            $products = DB::table('products')->latest()->paginate(5);
        }

        return new ApiResource(true, 'List Data Products', $products);
    }

    public function show(Product $product)
    {
        return new ApiResource(true, 'Data Product has been found', $product);
    }
}
