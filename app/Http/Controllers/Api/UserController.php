<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController as ApiBaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Auth as Auth;

class UserController extends ApiBaseController
{
    // User Register
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:8',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $input = $request->all();

        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);

        $success['token'] = $user->createToken('auth_token')->plainTextToken;
        $success['username'] = $user->username;

        return $this->sendResponse($success, 'User Registered Successfully');
    }

    // User Login
    public function login(Request $request)
    {

        if (!Auth::attempt($request->only('email', 'password'))) {
            return $this->sendError('Unauthorized', ['error' => 'Unauthorized']);
        }

        $user = User::where('email', $request['email'])->firstOrFail();
        $success['username'] = $user->username;;
        $success['token'] = $user->createToken('auth_token')->plainTextToken;
        return $this->sendResponse($success, 'User login successfully');
    }

    // User Logout
    public function logout(Request $request)
    {
        Auth::user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return $this->sendResponse([], 'Successfully logged out');
    }
}
